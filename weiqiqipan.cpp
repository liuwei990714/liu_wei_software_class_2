#include<graphics.h>
#include<conio.h>
int main()
{
	int step=30;
	//初始化绘图窗口
	initgraph(600,600);
	//设置背景色为黄色
	setbkcolor(YELLOW);
	//用背景色清空屏幕
	cleardevice();
	
	setlinestyle(SOLID_LINE,2);	//设置画线类型为实线，宽度为两个像素
	setcolor(RGB(0,0,0));	//设置前景颜色为黑色
	
	int i;
	for(i=1;i<=19;i++)
	{
		line(i*step,1*step,i*step,19*step);	//画横线 
		line(1*step,i*step,19*step,i*step);	//画竖线 
	 } 
	
	getch();
	closegraph();
	return 0; 
}
