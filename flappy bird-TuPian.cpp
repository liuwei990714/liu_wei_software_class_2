#include<graphics.h>
#include<conio.h>
#pragma comment(lib,"Winmm.lib")
IMAGE img_bk, img_bd1, img_bd2, img_bar_up1, img_bar_up2, img_bar_down1, img_bar_down2;
int bird_x, bird_y;

void startup()
{
	initgraph(350,600);
	loadimage(&img_bk, _T("E:\\background.jpg"));
	loadimage(&img_bd1, "E:\\bird1.jpg");
	loadimage(&img_bd2, "E:\\bird2.jpg");
	loadimage(&img_bar_up1, "E:\\bar_up1.gif");
	loadimage(&img_bar_up2, "E:\\bar_up2.gif");
	loadimage(&img_bar_down1, "E:\\bar_down1.gif");
	loadimage(&img_bar_down2, "E:\\bar_down2.gif");
	bird_x = 50;
	bird_y = 200;
	BeginBatchDraw();

	mciSendString("opoen D:\\background.mp3 alias bkmusic", NULL, 0, NULL);	//打开背景音乐
	mciSendString("play bkmusic repeat", NULL, 0, NULL);	//循环播放
}

void show()
{
	putimage(0, 0, &img_bk);	//显示背景
	putimage(150, -300, &img_bar_up1, NOTSRCERASE);	//显示上一半的障碍物
	putimage(150, -300, &img_bar_up2, SRCINVERT);	
	putimage(150, 400, &img_bar_down1, NOTSRCERASE);	//显示下一半的障碍物
	putimage(150, 400, &img_bar_down2, SRCINVERT);
	putimage(bird_x, bird_y, &img_bd1, NOTSRCERASE);	//显示小鸟
	putimage(bird_x, bird_y, &img_bd2, SRCINVERT);
	FlushBatchDraw();
	Sleep(50);
}

void updateWithoutInput()
{
	if (bird_y < 500)
		bird_y = bird_y + 3;
}

void updateWithInput()
{
	char input;
	if (_kbhit())
	{
		input = getch();
		if (input == ' ' && bird_y > 20)
			bird_y = bird_y - 60;
	}
}

void gameover()
{
	EndBatchDraw();
	_getch();
	closegraph();
}

int main()
{
	startup();
	while (1)
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	gameover();
	return 0;
}