#include<graphics.h>	//引用EasyX图形库 
#include<conio.h>
int main()
 {
 	initgraph(640,200);		//初始化640*480的画布
	 for(int y=0;y<=200;y=y+5)
	 {
	 	if(y/5%2==1)	//判断奇数行、偶数行 
	 		setcolor(RGB(255,0,0));	//设置前置颜色为纯红色 
	 	else
	 		setcolor(RGB(0,0,225));	//设置前置颜色为纯蓝色 
	 	line(0,y,640,y);	//画直线，（0，y）和（640，y）为直线两个端点的坐标 
	  } 
	  getch();	//按任意键继续 
	  closegraph();	//关闭图形界面 
	  return 0;
 }
