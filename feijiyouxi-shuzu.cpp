#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h> 

#define High 25	//游戏画面尺寸 
#define Width 50
#define EnemyNum 5	//敌机的个数 

//全局变量 
int position_x,position_y;	//飞机的位置 
int enemy_x[EnemyNum],enemy_y[EnemyNum];
	//敌机的位置 
int canvas[High][Width]={0};	//二维数组储存游戏画面中对应的元素
//0为空格，1为飞机* ，2为子弹|，3为敌机@ 

int score;	//得分 
int BulletWidth;	//子弹的宽度
int EnemyMoveSpeed;	//敌机的移动速度 

//消除屏闪，但不能消除光标闪烁问题 
void gotoxy(int x,int y)	//将光标移动到(x,y)位置 
{
	HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X=x;
	pos.Y=y;
	SetConsoleCursorPosition(handle,pos); 
}

//消除光标闪烁问题 
void HideCursor()
{
	CONSOLE_CURSOR_INFO cursor_info={1,0};
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE),&cursor_info);
 } 

void startup()
{
	position_x=High-1;
	position_y=Width/2;
	canvas[position_x][position_y]=1;
	int k;
	for(k=0;k<EnemyNum;k++)
	{
		enemy_x[k]=rand()%2;
		enemy_y[k]=rand()%Width;
		canvas[enemy_x[k]][enemy_y[k]]=3;
	}
	score=0;
	BulletWidth=0;
	EnemyMoveSpeed=20; 
 } 
 
  void show()				//显示画面 
 {
 	gotoxy(0,0)	;		//光标移动到原点位置，以下重画清屏 
 	int i,j;
 	for(i=0;i<High;i++)    //离上边界的距离 
 	{
 		for(j=0;j<Width;j++)  //离左边界的距离 
 		{
 			if(canvas[i][j]==0)
 				printf(" ");   //输出空格 
 			else if(canvas[i][j]==1)
			 	printf("*");	//输出飞机* 
			else if(canvas[i][j]==2)
				printf("|");	//输出子弹| 
			else if(canvas[i][j]==3)
				printf("@");	//输出敌机@ 
		 }
		 printf("\n");
	 }
	 printf("得分：%3d\n",score);
	 Sleep(20);
 }
 
void updateWithoutInput()     //与用户输入无关的更新 
{
	int i,j,k;
	for(i=0;i<High;i++)
	{
		for(j=0;j<Width;j++)
		{
			if(canvas[i][j]==2)	
			{
				for(k=0;k<EnemyNum;k++)
				{
					if((i==enemy_x[k])&&(j==enemy_y[k]))	//子弹击中飞机 
					{
					score++;	//分数加1 
					if(score%5==0&&EnemyMoveSpeed>3)	//达到一定积分后敌机变快
						EnemyMoveSpeed--;
					if(score%5==0)	//达到一定积分后子弹变厉害
						 BulletWidth++; 
					canvas[enemy_x[k]][enemy_y[k]]=0;
					enemy_x[k]=rand()%2;
					enemy_y[k]=rand()%Width;	//产生新的飞机 
					canvas[enemy_x[k]][enemy_y[k]]=3;
					canvas[i][j]=0;	//子弹消失 
					}
				}
				//子弹向上移动 
				canvas[i][j]=0;
				if(i>0)
					canvas[i-1][j]=2;
			}
		}
	 }
	 
	 for(k=0;k<EnemyNum;k++)
	 {
	 	if((position_x==enemy_x[k])&&(position_y==enemy_y[k]))	//敌机撞我机
	 	{
	 	printf("失败！\n");
	 	Sleep(2000);
	 	system("pause");
	 	exit(0);
	 	} 
	 
	 	if(enemy_x[k]>=High)	//敌机跑出显示屏幕 
	 	{
	 	canvas[enemy_x[k]][enemy_y[k]]=0;
	 	enemy_x[k]=rand()%2;	//产生新的飞机
		enemy_y[k]=rand()%Width;
		canvas[enemy_x[k]][enemy_y[k]]=3;
		score--;	//减分 
	 	}
	}
	 
	 static int speed=0;	//减缓敌机下落速度，只有当程序循环10次后，敌机才会下落一行，和Speed()的效果一样 
	 if(speed<EnemyMoveSpeed)
	 	speed++;
	if(speed==EnemyMoveSpeed)
	{
		for(k=0;k<EnemyNum;k++)
		{
			//敌机下落
		canvas[enemy_x[k]][enemy_y[k]]=0;
		enemy_x[k]++;
		canvas[enemy_x[k]][enemy_y[k]]=3;
		speed=0;
		}
	}
} 

 void updateWithInput()			//与用户输入有关的更新 
 {
 	char input;
 	if(kbhit())					//判断是否有输入 
 	{
 		input=getch();			//根据用户的不同输入来移动，不必回车 
 		if(input=='a')
		 {
		 	canvas[position_x][position_y]=0;
			position_y--;
			canvas[position_x][position_y]=1;  
		 } 
 		else if(input=='d')
 		{
 			canvas[position_x][position_y]=0;
			position_y++;
			canvas[position_x][position_y]=1; 
		 }
 		else if(input=='w')
 		{
 			canvas[position_x][position_y]=0;
			position_x--;
			canvas[position_x][position_y]=1; 
		 }
 		else if(input=='s')
 		{
 			canvas[position_x][position_y]=0;
			position_x++;
			canvas[position_x][position_y]=1; 
		 }
		 else if(input==' ')
		 {
		 	int left=position_y-BulletWidth;
		 	int right=position_y+BulletWidth;
		 	if(left<0)
		 		left=0;
		 	if(right>Width-1)
		 		right=Width-1;
		 	int k;
		 	for(k=left;k<=right;k++)
		 		canvas[position_x-1][k]=2;	//发射子弹的初始位置在飞机的正上方 
		 }
	 }
 }
 
  int main()
{
	HideCursor();
	startup(); //数据初始化 
	while(1)   //游戏不断循环显示 
	{
		show();//显示画面 
		updateWithoutInput();//与用户输入无关的更新 
		updateWithInput();//与用户输入有关的更新 
	}
	return 0;
}
