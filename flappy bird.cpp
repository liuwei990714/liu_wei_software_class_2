#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h>

//全局变量
int high,width;	//游戏画面大小
int bird_x,bird_y;	//小鸟的坐标
int bar1_y,bar1_xDown,bar1_xTop;	//障碍物的相关坐标 
int score;	//得分，经过障碍物的个数 

//消除屏闪，但不能消除光标闪烁问题 
void gotoxy(int x,int y)	//将光标移动到(x,y)位置 
{
	HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X=x;
	pos.Y=y;
	SetConsoleCursorPosition(handle,pos); 
}

void startup()
{
	high=20;
	width=50;
	bird_x=high/2;
	bird_y=3;
	bar1_y=width/2;
	bar1_xDown=high/3;
	bar1_xTop=high/2;
	score=0; 
}

void show()
{
	gotoxy(0,0);
	int i,j;
	for(i=0;i<high;i++)
	{
		for(j=0;j<width;j++)
		{
			if((i==bird_x)&&(j==bird_y))
				printf("@");	//输出小鸟 
			else if((j==bar1_y)&&((i>bar1_xTop)||(i<bar1_xDown)))
				printf("*");	//输出障碍物 
			else
				printf(" ");	//输出空格 
		}
		printf("\n");
	}
	printf("得分：%d\n",score);
}

void updateWithoutInput()
{
	bird_x++;
	bar1_y--;
	if(bird_y==bar1_y)
	{
		if((bird_x>=bar1_xDown)&&(bird_x<=bar1_xTop))
			score++;
		else
			{
				printf("游戏失败\n");
				system("pause");
				exit(0);
			}
	}
	if(bar1_y<0)	//重新生成一个障碍物 
	{
		bar1_y=width;
		int temp=rand()%int(high*0.8);
		bar1_xDown=temp-high/10;
		bar1_xTop=temp+high/10;
	}
	Sleep(300);
}

void updateWithInput()
{
	char input;
	if(kbhit())
	{
		input=getch();
		if(input==' ')
			bird_x=bird_x-2;
	}
}

int main()
{
	startup();
	while(1)
	{
		show();
		updateWithoutInput();
		updateWithInput();
	}
	return 0;
}
