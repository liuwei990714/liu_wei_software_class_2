#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<windows.h> 

//消除屏闪，但不能消除光标闪烁问题 
void gotoxy(int x,int y)	//将光标移动到(x,y)位置 
{
	HANDLE handle=GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos;
	pos.X=x;
	pos.Y=y;
	SetConsoleCursorPosition(handle,pos); 
}

//消除光标闪烁问题 
void HideCursor()
{
	CONSOLE_CURSOR_INFO cursor_info={1,0};
	SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE),&cursor_info);
 } 

//全局变量
int position_x,position_y;    //飞机位置 
int bullet_x,bullet_y;		  //子弹位置 
int enemy_x,enemy_y;		  //敌机位置 
int high,width;               //游戏画面尺寸 
int score;					  //得分 

void startup()				  //数据初始化 
{
	high=20;
	width=30;
	position_x=high/2;
	position_y=width/2;
	bullet_x=0;
	bullet_y=position_y; 
	enemy_x=0;
	enemy_y=position_y; 
	score=0;
 } 
 
 void show()				//显示画面 
 {
 	gotoxy(0,0)	;		//光标移动到原点位置，以下重画清屏 
 	int i,j;
 	for(i=0;i<high;i++)    //离上边界的距离 
 	{
 		for(j=0;j<width;j++)  //离左边界的距离 
 		{
 			if((i==position_x)&&(j==position_y))
 				printf("*");   //输出复杂的飞机图形 
 			else if((i==position_x+1)&&(j==position_y-2))
			 {
			 	printf("*****");
			 	j+=5;
			  } 
			else if((i==position_x+2)&&(j==position_y-1))
			{
				printf("* *");
				j+=3;
			}
 			else if((i==enemy_x)&&(j==enemy_y))
 				printf("@");	//输出敌机@ 
 			else if((i==bullet_x)&&(j==bullet_y))
 				printf("|");	//输出子弹 
 			else
			 	printf(" ");	
		 }
		 printf("\n");
	 }
	 printf("得分：%d\n",score);
 }
 
 void updateWithoutInput()     //与用户输入无关的更新 
 {
 	if(bullet_x>-1)
 		bullet_x--;
 	if((bullet_x==enemy_x)&&(bullet_y==enemy_y))	//子弹击中敌机 
 	{
 		score++;	//分数加1
		enemy_x=-1;	//产生新的敌机
		enemy_y=rand()%width;
		bullet_x=-2;	//子弹无效 
	 }
	 if(enemy_x>high)	//没有击中敌机，那么敌机到最后就会跑出显示画面 
	 {
	 	enemy_x=-1;		//产生新的敌机 
	 	enemy_y=rand()%width;
	 }
 	//不改变用户输入响应的频率前提下，将敌机的移动速度减慢。
	//用静态变量speed，每执行10次updateWithoutInput函数，敌机才移动一次。
	static int speed=0;
	if(speed<10)
		speed++;
	if(speed==10)
	{
		enemy_x++;
		speed=0;
	 } 
 }
 
 void updateWithInput()			//与用户输入有关的更新 
 {
 	char input;
 	if(kbhit())					//判断是否有输入 
 	{
 		input=getch();			//根据用户的不同输入来移动，不必回车 
 		if(input=='a')
 			position_y--;
 		if(input=='d')
 			position_y++;
 		if(input=='w')
 			position_x--;
 		if(input=='s')
 			position_x++;
 		if(input==' ')
 		{
 			bullet_x=position_x-1;
 			bullet_y=position_y;
		 }
	 }
 }
 
 int main()
{
	HideCursor();
	startup(); //数据初始化 
	while(1)   //游戏不断循环显示 
	{
		show();//显示画面 
		updateWithoutInput();//与用户输入无关的更新 
		updateWithInput();//与用户输入有关的更新 
	}
	return 0;
}
