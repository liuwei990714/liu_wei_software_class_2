#include<graphics.h>
#include<conio.h>
int main()
{
	initgraph(640, 480);
	MOUSEMSG m;	//定义鼠标消息
	while (1)
	{
		m = GetMouseMsg();	//获取一条鼠标消息
		if (m.uMsg == WM_MOUSEMOVE)
		{
			//鼠标移动的时候画白色小点
			putpixel(m.x, m.y, WHITE);
		}
		else if (m.uMsg == WM_LBUTTONDOWN)
		{
			//鼠标左键按下时在鼠标位置画一个方块
			rectangle(m.x - 5, m.y - 5, m.x + 5, m.y + 5);
		}
		else if (m.uMsg == WM_RBUTTONUP)
		{
			//鼠标右键按下又抬起时画一个圆
			circle(m.x, m.y, 10);
		}
	}
	return 0;
}